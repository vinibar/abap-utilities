*--------------------------------------------------------------------*
* &. ZMineSwepper
*--------------------------------------------------------------------*
* &. Developed: Vinícius Barrionuevo
* &. Data: 01.06.2017 14:08:26
*--------------------------------------------------------------------*
* &. Obs:
*
*--------------------------------------------------------------------*
report zvab_mineswepper.


class lcl_app definition deferred.
class lcl_mineswepper definition deferred.

data o_app type ref to lcl_app.

data teste type c.

parameters:
  p_easy radiobutton group g1,
  p_medium radiobutton group g1,
  p_hard radiobutton group g1.

parameters:
  p_level no-display.

*----------------------------------------------------------------------*
*       CLASS lcl_mineswepper DEFINITION
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
class lcl_mineswepper definition.

  public section.
    constants:
      level_easy type c value '1',
      level_medium type c value '2',
      level_hard type c value '3',
      mines_easy type i value 10,
      mines_medium type i value 40,
      mines_hard type i value 99,
      size_easy type i value 9,
      size_medium type i value 16,
      size_hard type i value 24,
      flag type icon-id value '@8R@',
      mine type icon-id value '@0W@'.

    types:
      begin of ty_cleared_cells,
        col_id type lvc_s_col-fieldname,
        row_id type lvc_s_row-index,
      end of ty_cleared_cells,

      ty_t_cleared_cells type table of ty_cleared_cells.

    methods:
      constructor
        importing
          i_level type c,

      create_board
        exporting
            er_board type ref to data
            et_fieldcat type lvc_t_fcat
            es_layout type lvc_s_layo,

      insert_mines,

      get_board
        exporting
            er_board type ref to data,

      get_cell_value
        importing
          i_column_id type lvc_s_col
          i_row_id type lvc_s_row
        returning value(rv_value) type char10,

      get_mined_board
        importing
          i_column_id type lvc_s_col-fieldname
          i_row_id type lvc_s_row-index
        exporting
          et_mined_board type standard table,

      get_cleared_board
          importing
            i_column_id type lvc_s_col-fieldname
            i_row_id type lvc_s_row-index
          changing
            ch_cleared_cells type ty_t_cleared_cells optional.


  private section.
    data: level type c.
    data: board type ref to data.
    data: mined_board type ref to data.
    data: size type i.
    data: mines type i.
    data: style_fname type lvc_fname.


endclass.                    "lcl_mineswepper DEFINITION

*----------------------------------------------------------------------*
*       CLASS lcl_mineswepper IMPLEMENTATION
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
class lcl_mineswepper implementation.

  method constructor.

    level = i_level.

    if level = lcl_mineswepper=>level_easy.
      size = lcl_mineswepper=>size_easy.
      mines = lcl_mineswepper=>mines_easy.
    elseif level = lcl_mineswepper=>level_medium.
      size = lcl_mineswepper=>size_medium.
      mines = lcl_mineswepper=>mines_medium.
    else.
      size = lcl_mineswepper=>size_hard.
      mines = lcl_mineswepper=>mines_hard.
    endif.
  endmethod.                    "constructor

  method create_board.

    data lv_count type i.

    field-symbols <l_board> type standard table.

    field-symbols <fieldcat> like line of et_fieldcat.
    do me->size times.
      add 1 to lv_count.
      append initial line to et_fieldcat assigning <fieldcat>.
      <fieldcat>-fieldname = |C{ lv_count }|.
      <fieldcat>-datatype = 'CHAR'.
      <fieldcat>-inttype = 'CHAR'.
      <fieldcat>-dd_outlen = 2.
      <fieldcat>-intlen = 4.
      <fieldcat>-icon = abap_true.
      <fieldcat>-hotspot = abap_true.
    enddo.

    append initial line to et_fieldcat assigning <fieldcat>.
    <fieldcat>-fieldname = 'T_CELLCOLORS'.
    <fieldcat>-tech = abap_true.
    <fieldcat>-ref_field = 'COLTAB'.
    <fieldcat>-ref_table = 'CALENDAR_TYPE'.

    cl_alv_table_create=>create_dynamic_table(
        exporting
            it_fieldcatalog = et_fieldcat
            i_style_table = abap_true
        importing
            ep_table        = board
            e_style_fname   = style_fname ).


    assign board->* to <l_board>.

    data lt_style type lvc_t_styl.
    data ls_style type lvc_s_styl.

    do me->size times.
      field-symbols <ls_board> type any.
      append initial line to <l_board> assigning <ls_board>.
      if lt_style is initial.
        clear lv_count.
        do me->size times.
          add 1 to lv_count.
          ls_style-fieldname = |C{ lv_count }|.
          ls_style-style = cl_gui_alv_grid=>mc_style_disabled.
          insert ls_style into table lt_style.
        enddo.
      endif.

      field-symbols <l_field> type any.
      assign component style_fname of structure <ls_board> to <l_field>.
      <l_field> = lt_style.
    enddo.

    es_layout-stylefname = style_fname.
    es_layout-no_rowmark = abap_true.
    es_layout-sel_mode = 'A'.
    es_layout-no_headers = abap_true.
    es_layout-ctab_fname = 'T_CELLCOLORS'.

    cl_alv_table_create=>create_dynamic_table(
        exporting
            it_fieldcatalog = et_fieldcat
            i_style_table = abap_true
        importing
            ep_table        = mined_board
            e_style_fname   = style_fname ).

    field-symbols <l_mined_board> type standard table.
    assign mined_board->* to <l_mined_board>.
    <l_mined_board> = <l_board> .

    er_board = me->board.

  endmethod.                    "create_board

  method insert_mines.

    field-symbols <l_board> type standard table.
    field-symbols <ls_board> type any.
    field-symbols <l_field> type any.

    data lv_col type i.
    data lv_row type i.
    data lv_fieldname type lvc_fname.

    assign mined_board->* to <l_board>.

    do me->mines times.

      do.
        call function 'GENERAL_GET_RANDOM_INT'
          exporting
            range  = me->size
          importing
            random = lv_col.

        if lv_col <> 0.
          exit.
        endif.
      enddo.

      do.
        call function 'GENERAL_GET_RANDOM_INT'
          exporting
            range  = me->size
          importing
            random = lv_row.
        if lv_col <> 0.
          exit.
        endif.
      enddo.

      data lv_fieldname_near like lv_fieldname.
      data lv_start_row type i.
      data lv_end_row type i.

      read table <l_board> assigning <ls_board> index lv_row.
      if sy-subrc is initial.
        lv_fieldname = |C{ lv_col }|.
        assign component lv_fieldname of structure <ls_board> to <l_field>.
        <l_field> = lcl_mineswepper=>mine.

        lv_start_row = lv_row - 1.
        if lv_start_row <= 0.
          lv_start_row = 1.
        endif.

        lv_end_row = lv_row + 1.
        if lv_end_row > me->size.
          lv_end_row = me->size.
        endif.

        loop at <l_board> assigning <ls_board> from lv_start_row to lv_end_row.

          if lv_col - 1 > 0.
            lv_fieldname_near = |C{ lv_col - 1 }|.
            assign component lv_fieldname_near of structure <ls_board> to <l_field>.
            if <l_field> <> lcl_mineswepper=>mine.
              add 1 to <l_field>.
            endif.
          endif.

          lv_fieldname_near = |C{ lv_col }|.
          assign component lv_fieldname_near of structure <ls_board> to <l_field>.
          if <l_field> <> lcl_mineswepper=>mine.
            add 1 to <l_field>.
          endif.

          if lv_col + 1 <= me->size.
            lv_fieldname_near = |C{ lv_col + 1 }|.
            assign component lv_fieldname_near of structure <ls_board> to <l_field>.
            if <l_field> <> lcl_mineswepper=>mine.
              add 1 to <l_field>.
            endif.
          endif.

        endloop.
      endif.
    enddo.

  endmethod.                    "insert_mines

  method get_board.

    er_board = board.

  endmethod.                    "get_board

  method get_cell_value.

    field-symbols <l_mined_board> type standard table.
    field-symbols <l_line> type any.
    field-symbols <l_field> type any.

    assign me->mined_board->* to <l_mined_board>.
    read table <l_mined_board> assigning <l_line> index i_row_id.
    if sy-subrc is initial.
      assign component i_column_id of structure <l_line> to <l_field>.
      rv_value = <l_field>.
    endif.


  endmethod.                    "get_cell_value

  method get_mined_board.

    field-symbols <l_mined_board> type standard table.
    field-symbols <ls_line> type any.
    field-symbols <lt_colors> type lvc_t_scol.
    data ls_color type lvc_s_scol.

    assign mined_board->* to <l_mined_board>.

    read table <l_mined_board> assigning <ls_line> index i_row_id.
    if sy-subrc is initial.
      assign component 'T_CELLCOLORS' of structure <ls_line> to <lt_colors>.
      ls_color-fname = i_column_id.
      ls_color-color-col = 6.
      ls_color-color-int = 1.
      insert ls_color into table <lt_colors>.
    endif.

    et_mined_board = <l_mined_board>.


  endmethod.                    "get_mined_board

  method get_cleared_board.

    data lv_start_row type i.
    data lv_end_row type i.
    data lv_fieldname_near type lvc_s_col-fieldname.

    field-symbols <l_board> type standard table.
    field-symbols <l_mined_board> type standard table.
    field-symbols <ls_board> type any.
    field-symbols <ls_mined_board> type any.
    field-symbols <l_field_board> type any.
    field-symbols <l_field_mined_board> type any.
    data lv_col type i.

    field-symbols <l_colors> type lvc_t_scol.
    data ls_color type lvc_s_scol.

    lv_start_row = i_row_id - 1.
    if lv_start_row <= 0.
      lv_start_row = 1.
    endif.

    lv_end_row = i_row_id + 1.
    if lv_end_row > me->size.
      lv_end_row = me->size.
    endif.

    lv_col = i_column_id+1.

    assign board->* to <l_board>.
    assign mined_board->* to <l_mined_board>.

    data lv_current_col type i.
    data lv_current_row type lvc_s_row-index.

    field-symbols <l_cleared_cell> like line of ch_cleared_cells.
    append initial line to ch_cleared_cells assigning <l_cleared_cell>.
    <l_cleared_cell>-row_id = i_row_id.
    <l_cleared_cell>-col_id = i_column_id.

    loop at <l_mined_board> assigning <ls_mined_board> from lv_start_row to lv_end_row.

      lv_current_row = sy-tabix.

      read table <l_board> assigning <ls_board> index sy-tabix.
      if sy-subrc is initial.

        lv_current_col = lv_col - 1.
        if lv_current_col > 0.
          lv_fieldname_near = |C{ lv_current_col }|.
          assign component lv_fieldname_near of structure <ls_mined_board> to <l_field_mined_board>.
          assign component lv_fieldname_near of structure <ls_board> to <l_field_board>.
          if <l_field_mined_board> is initial.

            assign component 'T_CELLCOLORS' of structure <ls_board> to <l_colors>.

            ls_color-fname = lv_fieldname_near.
            ls_color-color-col = 1.
            insert ls_color into table <l_colors>.

            read table ch_cleared_cells transporting no fields
              with key col_id = lv_fieldname_near
                       row_id = lv_current_row.
            if sy-subrc is not initial.
              get_cleared_board(
                exporting
                  i_column_id = lv_fieldname_near
                  i_row_id = lv_current_row
                changing
                  ch_cleared_cells = ch_cleared_cells ).
            endif.
          else.
            <l_field_board> = <l_field_mined_board>.
          endif.
        endif.

        lv_current_col = lv_col.
        lv_fieldname_near = |C{ lv_current_col }|.
        assign component lv_fieldname_near of structure <ls_mined_board> to <l_field_mined_board>.
        assign component lv_fieldname_near of structure <ls_board> to <l_field_board>.
        if <l_field_mined_board> is initial.

          assign component 'T_CELLCOLORS' of structure <ls_board> to <l_colors>.

          ls_color-fname = lv_fieldname_near.
          ls_color-color-col = 1.
          insert ls_color into table <l_colors>.

          read table ch_cleared_cells transporting no fields
            with key col_id = lv_fieldname_near
                     row_id = lv_current_row.
          if sy-subrc is not initial.
            get_cleared_board(
              exporting
                i_column_id = lv_fieldname_near
                i_row_id = lv_current_row
              changing
                ch_cleared_cells = ch_cleared_cells ).
          endif.
        else.
          <l_field_board> = <l_field_mined_board>.
        endif.

        lv_current_col = lv_col + 1.
        if  lv_current_col <= me->size.
          lv_fieldname_near = |C{ lv_current_col }|.
          assign component lv_fieldname_near of structure <ls_mined_board> to <l_field_mined_board>.
          assign component lv_fieldname_near of structure <ls_board> to <l_field_board>.
          if <l_field_mined_board> is initial.

            assign component 'T_CELLCOLORS' of structure <ls_board> to <l_colors>.

            ls_color-fname = lv_fieldname_near.
            ls_color-color-col = 1.
            insert ls_color into table <l_colors>.

            read table ch_cleared_cells transporting no fields
              with key col_id = lv_fieldname_near
                       row_id = lv_current_row.
            if sy-subrc is not initial.
              get_cleared_board(
                exporting
                  i_column_id = lv_fieldname_near
                  i_row_id = lv_current_row
                changing
                  ch_cleared_cells = ch_cleared_cells ).
            endif.
          else.
            <l_field_board> = <l_field_mined_board>.
          endif.
        endif.

      endif.

    endloop.


  endmethod.                    "get_cleared_board

endclass.                    "lcl_mineswepper IMPLEMENTATION

*----------------------------------------------------------------------*
*       CLASS lcl_app DEFINITION
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
class lcl_app definition create private.

  public section.
    class-methods: factory
      returning value(r_instance) type ref to lcl_app.

    methods:
      main,
      at_selection_screen,
      pbo,
      pai
        importing value(i_ucomm) type sy-ucomm,
      exit
        importing value(i_ucomm) type sy-ucomm,

      on_context_menu for event context_menu_request of cl_gui_alv_grid
        importing
            e_object,

      on_hotspot_click for event hotspot_click of cl_gui_alv_grid
        importing
          e_column_id
          e_row_id
          es_row_no.

*      on_button_click for event button_click of cl_gui_alv_grid
*        importing
*          es_row_no.

  private section.
    class-data: o_instance type ref to lcl_app.
    data o_mineswepper type ref to lcl_mineswepper.
    data o_container type ref to cl_gui_docking_container.
    data o_alv type ref to cl_gui_alv_grid.



endclass.                    "lcl_app DEFINITION

*----------------------------------------------------------------------*
*       CLASS lcl_app IMPLEMENTATION
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
class lcl_app implementation..

  method factory.

    if o_instance is not bound.
      create object o_instance.
    endif.

    r_instance = o_instance.

  endmethod.                    "factory

  method at_selection_screen.

    if p_easy is not initial.
      p_level = lcl_mineswepper=>level_easy.
    endif.
    if p_medium is not initial.
      p_level = lcl_mineswepper=>level_medium.
    endif.
    if p_hard is not initial.
      p_level = lcl_mineswepper=>level_hard.
    endif.

  endmethod.                    "at_selection_screen

  method main.
    call screen 9000.
  endmethod.                    "main

  method pbo.

    data lr_board type ref to data.
    field-symbols <l_board> type standard table.
    data lt_fieldcat type lvc_t_fcat.
    data ls_layout type lvc_s_layo.
    set pf-status '9000'.
    set titlebar '9000'.

    if o_mineswepper is not bound.
      create object o_mineswepper
        exporting
          i_level = p_level.

      o_mineswepper->create_board(
        importing
          er_board = lr_board
          et_fieldcat = lt_fieldcat
          es_layout = ls_layout
      ).

      o_mineswepper->insert_mines( ).

    endif.

    if o_alv is not bound.
      create object o_container
        exporting
          extension = 9999.

      create object o_alv
        exporting
          i_parent = o_container.

      assign lr_board->* to <l_board>.



      set handler:
        on_context_menu for o_alv,
        on_hotspot_click for o_alv.
*        on_button_click for o_alv.

      o_alv->set_table_for_first_display(
       exporting
        is_layout = ls_layout
       changing
       it_fieldcatalog = lt_fieldcat
       it_outtab = <l_board>
      ).
    endif.


  endmethod.                    "pbo

  method pai.

    case i_ucomm.
      when others.
    endcase.

  endmethod.                    "pai

  method exit.
    case i_ucomm.
      when 'E' or 'ENDE'.
        leave to screen 0.
      when 'ENDE'.
        leave program.
    endcase.
  endmethod.                    "exit

  method on_context_menu.
    data lt_cells type lvc_t_cell.

    e_object->clear( ).
    o_alv->get_selected_cells(
        importing
        et_cell = lt_cells
    ).

    field-symbols <cell> like line of lt_cells.
    field-symbols <l_board> type standard table.
    field-symbols <l_line> type any.
    field-symbols <l_field> type any.
    read table lt_cells assigning <cell> index 1.
    if sy-subrc is initial.

      data lr_board type ref to data.
      o_mineswepper->get_board(
        importing
            er_board = lr_board
      ).

      assign lr_board->* to <l_board>.
      read table <l_board> assigning <l_line> index <cell>-row_id.
      if sy-subrc is initial.
        assign component <cell>-col_id of structure <l_line> to <l_field>.
        if <l_field> is initial.
          <l_field> = lcl_mineswepper=>flag.
        elseif <l_field> = lcl_mineswepper=>flag.
          clear <l_field>.
        endif.
      endif.

    endif.

    o_alv->refresh_table_display( ).

  endmethod.                    "on_context_menu

  method on_hotspot_click.

    data lr_board type ref to data.
    field-symbols <l_board> type standard table.
    field-symbols <l_line> type any.
    field-symbols <l_field> type any.

    o_mineswepper->get_board(
      importing
        er_board = lr_board
    ).

    assign lr_board->* to <l_board>.
    read table <l_board> assigning <l_line> index e_row_id-index.
    if sy-subrc is initial.
      assign component e_column_id of structure <l_line> to <l_field>.

      if <l_field> = lcl_mineswepper=>flag.
        return.
      endif.

      <l_field> = o_mineswepper->get_cell_value(
                    i_column_id = e_column_id
                    i_row_id = e_row_id ).

      if <l_field> = lcl_mineswepper=>mine.
        o_mineswepper->get_mined_board(
          exporting
            i_column_id = e_column_id-fieldname
            i_row_id = e_row_id-index
          importing
            et_mined_board = <l_board>
        ).
      endif.

      if <l_field> is assigned.
        if <l_field> = space.

          o_mineswepper->get_cleared_board(
            exporting
              i_column_id = e_column_id-fieldname
              i_row_id = e_row_id-index
          ).

        endif.
      endif.

    endif.

    o_alv->refresh_table_display( ).

  endmethod.                    "on_hotspot_click

*  method on_button_click.
*
*    BREAK-POINT.
*
*  endmethod.

endclass.                    "lcl_app IMPLEMENTATION

at selection-screen.
  o_app->at_selection_screen( ).

initialization.
  o_app = lcl_app=>factory( ).

start-of-selection.

  o_app->main( ).


*----------------------------------------------------------------------*
*  MODULE pbo_9000 OUTPUT
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
module pbo_9000 output.
  o_app->pbo( ).
endmodule.                    "pbo_9000 OUTPUT

*----------------------------------------------------------------------*
*  MODULE pai_9000 INPUT
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
module pai_9000 input.
  o_app->pai( sy-ucomm ).
endmodule.                    "pai_9000 INPUT

*----------------------------------------------------------------------*
*  MODULE pai_e_9000 INPUT
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
module pai_e_9000 input.
  o_app->exit( sy-ucomm ).
endmodule.                    "pai_e_9000 INPUT
