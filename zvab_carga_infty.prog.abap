*--------------------------------------------------------------------*
*
*                     == Carga de Infotipos ==
*
*--------------------------------------------------------------------*
* Developed by:
*  - Vinicius Barrionuevo (@vinibar) / vinicius21@gmail.com
*--------------------------------------------------------------------*
report yhrbr_efd_esocial_carga_infty.

tables sscrfields.

selection-screen: function key 1.

selection-screen: begin of block b1 with frame title text-t01.
parameters:
  p_infty type prelp-infty,
  p_subty type prelp-subty.
selection-screen: end of block b1.

selection-screen: begin of block b2 with frame title text-t02.
parameters:
  p_file type char255,
  p_sep default ';' obligatory.
selection-screen: end of block b2.

parameters:
  p_prod as checkbox.

*----------------------------------------------------------------------*
*       CLASS lcl_log DEFINITION
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
class lcl_log definition create private.

  public section.

    class-methods:
        factory
            importing
                i_extnumber type bal_s_log-extnumber
                i_object type bal_s_log-object optional
                i_aldate type bal_s_log-aldate
                i_altime type bal_s_log-altime
                i_aluser type bal_s_log-aluser
                i_alprog type bal_s_log-alprog
            changing
                ch_log type ref to lcl_log.

    methods:
        add_log_from_bapireturn
            importing
                i_pernr type pernr_d
                i_return type bapireturn1,
        add_log
          importing
            i_msgty type sy-msgty
            i_pernr type pernr_d
            i_msg type sy-msgv1,


    display.

  private section.

    class-data: o_instance type ref to lcl_log.
    data s_log_handle type balloghndl.

    methods:
        constructor
            importing
                i_bal_log type bal_s_log.

endclass.                    "lcl_log DEFINITION

*----------------------------------------------------------------------*
*       CLASS lcl_log IMPLEMENTATION
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
class lcl_log implementation.

  method factory.

    data ls_bal_log type bal_s_log.

    ls_bal_log-extnumber = i_extnumber.
    ls_bal_log-object = i_object.
    ls_bal_log-aldate = i_aldate.
    ls_bal_log-altime = i_altime.
    ls_bal_log-aluser = i_aluser.
    ls_bal_log-alprog = i_alprog.

    create object o_instance
      exporting
        i_bal_log = ls_bal_log.

    ch_log = o_instance.

  endmethod.                    "factory

  method constructor.

    call function 'BAL_LOG_CREATE'
      exporting
        i_s_log                 = i_bal_log
      importing
        e_log_handle            = s_log_handle
      exceptions
        log_header_inconsistent = 1
        others                  = 2.

  endmethod.                    "constructor

  method add_log_from_bapireturn.

    data ls_log type bal_s_msg.

    ls_log-msgty = i_return-type.
    ls_log-msgid = i_return-id.
    ls_log-msgno = i_return-log_msg_no.
    ls_log-msgv1 = i_return-message_v1.
    ls_log-msgv2 = i_return-message_v2.
    ls_log-msgv3 = i_return-message_v3.
    ls_log-msgv4 = i_return-message_v4.

    call function 'BAL_LOG_MSG_ADD'
      exporting
        i_log_handle     = s_log_handle
        i_s_msg          = ls_log
      exceptions
        log_not_found    = 1
        msg_inconsistent = 2
        log_is_full      = 3
        others           = 4.

  endmethod.                    "add_log_from_bapireturn

  method add_log.

    data ls_log type bal_s_msg.

    ls_log-msgty = i_msgty.
    ls_log-msgid = '00'.
    ls_log-msgno = '398'.
    ls_log-msgv1 = i_pernr && ': '.
    ls_log-msgv2 = i_msg.

    call function 'BAL_LOG_MSG_ADD'
      exporting
        i_log_handle     = s_log_handle
        i_s_msg          = ls_log
      exceptions
        log_not_found    = 1
        msg_inconsistent = 2
        log_is_full      = 3
        others           = 4.

  endmethod.                    "add_log

  method display.

    call function 'BAL_DSP_LOG_DISPLAY'
      exceptions
        profile_inconsistent = 1
        internal_error       = 2
        no_data_available    = 3
        no_authority         = 4
        others               = 5.

    if sy-subrc <> 0.
      message id sy-msgid type sy-msgty number sy-msgno
         with sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
    endif.

  endmethod.                    "display

endclass.                    "lcl_log IMPLEMENTATION

*----------------------------------------------------------------------*
*       CLASS lcl_carga_infty DEFINITION
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
class lcl_carga_infty definition create private.

  public section.

    types: ty_file_tab type table of string with default key.

    constants:
               c_ucomm_download_template type sscrfields-ucomm value 'FC01'.

    class-methods:
        get_instance returning value(r_instance) type ref to lcl_carga_infty.

    methods:
        at_selection_screen_for_file
            returning value(r_file) type string,
        start_of_selection,
      at_selection_screen_ucomm
        importing
          i_ucomm type sscrfields-ucomm,
      load_selscreen_texts,
      at_selection_screen_for_subty
        returning
          value(r_result) type prelp-subty.

  private section.

    class-data o_instance type ref to lcl_carga_infty.
    methods upload_file
      importing
        i_p_file type char255
      returning
        value(rt_file) type ty_file_tab.

    methods create_table_structure
      returning
        value(rr_infty) type ref to data.

    methods move_file_tab_to_infty
      importing
        it_file_tab type lcl_carga_infty=>ty_file_tab
        io_log type ref to lcl_log
      changing
        cr_infty type ref to data.

    methods set_mandatory_infty_fields
      changing
        cr_infty type ref to data.

    methods download_template.

endclass.                    "lcl_carga_infty DEFINITION

*----------------------------------------------------------------------*
*       CLASS lcl_carga_infty IMPLEMENTATION
*----------------------------------------------------------------------*
*
*----------------------------------------------------------------------*
class lcl_carga_infty implementation.

  method get_instance.

    if o_instance is not bound.
      create object o_instance.
    endif.

    r_instance = o_instance.

  endmethod.                    "get_instance

  method at_selection_screen_for_file.

    data lt_files type filetable.
    data lv_rc type sy-subrc.

    cl_gui_frontend_services=>file_open_dialog(
      exporting
        default_extension       = 'csv'    " Default Extension
        file_filter             = 'CSV Files (*.csv)|*.csv'
        initial_directory       = 'C:\'    " Initial Directory
        multiselection          = space
      changing
        file_table              = lt_files    " Table Holding Selected Files
        rc                      = lv_rc    " Return Code, Number of Files or -1 If Error Occurred
      exceptions
        file_open_dialog_failed = 1
        cntl_error              = 2
        error_no_gui            = 3
        not_supported_by_gui    = 4
    ).

    field-symbols <file> like line of lt_files.
    read table lt_files assigning <file> index 1.
    if sy-subrc is initial.
      r_file = <file>-filename.
    endif.

  endmethod.                    "at_selection_screen_for_file


  method start_of_selection.

    data lt_file_tab type lcl_carga_infty=>ty_file_tab.
    data lo_log type ref to lcl_log.
    data lr_infty type ref to data.
    data ls_return type bapireturn1.
    data lv_nocommit type sap_bool.

    field-symbols <t_infty> type standard table.
    field-symbols <infty> type any.

    if p_infty is initial.
      message 'Infotipo deve ser preenchido' type 'S' display like 'E'.
      leave list-processing.
    endif.


    if p_subty is initial.
      data lv_subtipo_obrigatorio type t582a-sbtex.
      select single sbtex from t582a
        into lv_subtipo_obrigatorio
        where infty = p_infty.

      if lv_subtipo_obrigatorio = abap_true.
        message 'Subtipo deve ser preenchido' type 'S' display like 'E'.
        leave list-processing.
      endif.
    endif.

    if p_file is initial.
      message 'Caminho do arquivo deve ser preenchido' type 'S' display like 'E'.
      leave list-processing.
    endif.


    lcl_log=>factory(
      exporting
        i_extnumber = 'Carga Infotipo'
        i_aldate    = sy-datum
        i_altime    = sy-uzeit
        i_aluser    = sy-uname
        i_alprog    = sy-repid
      changing
        ch_log      = lo_log
    ).

    lt_file_tab = me->upload_file( p_file ).
    lr_infty = me->create_table_structure( ).

    me->move_file_tab_to_infty(
        exporting
            it_file_tab = lt_file_tab
            io_log = lo_log
        changing
            cr_infty = lr_infty
    ).

    me->set_mandatory_infty_fields(
        changing
            cr_infty = lr_infty
    ).

    assign lr_infty->* to <t_infty>.


    field-symbols <pernr> type prelp-pernr.
    field-symbols <subty> type prelp-subty.

    loop at <t_infty> assigning <infty>.

      assign component 'PERNR' of structure <infty> to <pernr>.
      assign component 'SUBTY' of structure <infty> to <subty>.

      call function 'BAPI_EMPLOYEE_ENQUEUE'
        exporting
          number = <pernr>.

      if p_prod = abap_true.
        call function 'HR_PSBUFFER_INITIALIZE'.
        lv_nocommit = abap_false.
      else.
        lv_nocommit = abap_true.
      endif.

      call function 'HR_INFOTYPE_OPERATION'
        exporting
          infty     = p_infty
          number    = <pernr>
          subtype   = <subty>
          record    = <infty>
          operation = 'INS'
          nocommit  = lv_nocommit
        importing
          return    = ls_return.

      call function 'BAPI_EMPLOYEE_DEQUEUE'
        exporting
          number = <pernr>.

      if ls_return is initial.
        lo_log->add_log(
          i_pernr = <pernr>
          i_msg = 'Registro inserido com sucesso'
          i_msgty = 'S'
        ).
      else.
        lo_log->add_log_from_bapireturn(
          i_pernr = <pernr>
          i_return = ls_return
        ).
      endif.


    endloop.

    lo_log->display( ).


  endmethod.                    "start_of_selection


  method upload_file.

    data lv_filename type string.

    lv_filename = p_file.

    cl_gui_frontend_services=>gui_upload(
      exporting
        filename                = lv_filename
      changing
        data_tab                = rt_file ).

  endmethod.                    "upload_file


  method create_table_structure.

    data lv_infty_structure type char5.

    concatenate 'P' p_infty into lv_infty_structure.
    create data rr_infty type table of (lv_infty_structure).

  endmethod.                    "create_table_structure


  method move_file_tab_to_infty.

    data lt_splitted_line type table of string.
    data lt_columns type table of string.
    field-symbols <file> like line of it_file_tab.
    field-symbols <t_infty> type standard table.
    field-symbols <t_infty_def> type standard table.
    field-symbols <infty> type any.
    field-symbols <field> type any.
    field-symbols <line> like line of lt_splitted_line.
    field-symbols <column> like line of lt_columns.
    field-symbols <pernr> type pernr_d.
    data lv_field_type type c.
    data lv_datum type sy-datum.


    " Move valores para os campos do infotipo
    assign cr_infty->* to <t_infty>.

    read table it_file_tab assigning <file> index 2.
    if sy-subrc is initial.
      split <file> at p_sep into table lt_columns.
    endif.

    loop at it_file_tab assigning <file> from 3.

      split <file> at p_sep into table lt_splitted_line.

      append initial line to <t_infty> assigning <infty>.

      loop at lt_columns assigning <column>.

        data lv_offset type i.
        find regex '\-(.*)' in <column> match offset lv_offset.
        add 1 to lv_offset.

        assign component <column>+lv_offset of structure <infty> to <field>.

        if sy-subrc is initial.

          read table lt_splitted_line assigning <line> index sy-tabix.
          if sy-subrc is initial.

            describe field <field> type lv_field_type.
            if lv_field_type = 'D'.
              replace all occurrences of '/' in <line> with '.'.

              try.

                  call method cl_abap_datfm=>conv_date_ext_to_int(
                    exporting
                      im_datext   = <line>
                      im_datfmdes = '1'
                    importing
                      ex_datint   = lv_datum
                                    ).
                catch
                 cx_abap_datfm_format_unknown
                 cx_abap_datfm_invalid_date
                 cx_abap_datfm_no_date
                 cx_abap_datfm_ambiguous.

                  assign component 'PERNR' of structure <infty> to <pernr>.
                  if <pernr> is assigned.
                    io_log->add_log(
                      exporting
                        i_msgty = 'E'
                        i_pernr = <pernr>
                        i_msg   = 'Formato de data inválido'
                    ).
                  endif.

              endtry.

              <line> = lv_datum.

            endif.

            <field> = <line>.
          endif.
        endif.
      endloop.

    endloop.

  endmethod.                    "move_file_tab_to_infty


  method set_mandatory_infty_fields.

    field-symbols <t_infty> type any table.
    field-symbols <infty> type any.
    field-symbols <field> type any.
    data lv_namst type t777d-namst.

    assign cr_infty->* to <t_infty>.

    loop at <t_infty> assigning <infty>.

      assign component 'INFTY' of structure <infty> to <field>.
      if sy-subrc is initial.
        <field> = p_infty.
      endif.

      assign component 'SUBTY' of structure <infty> to <field>.
      if sy-subrc is initial.
        <field> = p_subty.
      endif.

      select single namst from t777d
        into lv_namst
        where infty = p_infty.

      assign component lv_namst of structure <infty> to <field>.
      <field> = p_subty.

    endloop.

  endmethod.                    "set_mandatory_infty_fields


  method at_selection_screen_ucomm.

    data lv_subtipo_obrigatorio type t582a-sbtex.

    case i_ucomm.
      when c_ucomm_download_template.
        if p_infty is initial.
          message 'Campo Infotipo deve ser preenchido' type 'W' display like 'E'.
          return.
        endif.

        select single sbtex from t582a
          into lv_subtipo_obrigatorio
          where infty = p_infty.

        if lv_subtipo_obrigatorio = abap_true.
          message 'Campo subtipo deve ser preenchido' type 'W' display like 'E'.
          return.
        endif.

        me->download_template( ).

    endcase.

  endmethod.                    "at_selection_screen_ucomm


  method load_selscreen_texts.

    sscrfields-functxt_01 = 'Download de layout'.

  endmethod.                    "load_selscreen_texts


  method download_template.

    types:
        begin of ty_fields,
            sname type t588mfprops-sname,
            fname type t588mfprops-fname,
        end of ty_fields.

    data lt_out type table of string.
    data lt_fields type table of ty_fields.

    data ls_fields type string.
    data ls_fields_description type string.
    data ls_metadata type hrpad_field_metadata.

    data lv_tabname type ddobjname.
    data lv_fname type dfies-fieldname.
    data lv_filename type string.
    data lv_dummy type string.
    data lv_text type string.
    data lv_tabix type sy-tabix.

    data lt_fies_tab type table of dfies.
    data lt_files type t588mfprops.
    field-symbols <fies> like line of lt_fies_tab.
    field-symbols <field> like line of lt_fields.

    cl_gui_frontend_services=>file_save_dialog(
      exporting
        default_extension       = 'csv'    " Default Extension
        file_filter             = 'CSV Files (*.csv)|*.csv'
        initial_directory       = 'C:\'    " Initial Directory
      changing
        filename = lv_dummy
        path = lv_dummy
        fullpath = lv_filename
       exceptions
        others = 99
    ).

    data lt_q588m type table of q588m.

    lt_q588m = zvab_cl_t588m=>read_t588m(
        iv_infty = p_infty
        iv_subty = p_subty
    ).

    data lv_str_infty type string.
    lv_str_infty = 'P' && p_infty && '-'.

    ls_fields = lv_str_infty && 'PERNR' && p_sep.
    ls_fields_description = 'Matrícula' && p_sep.
    ls_fields = ls_fields && lv_str_infty && 'BEGDA' && p_sep.
    ls_fields_description = ls_fields_description && 'Dt. Início' && p_sep.
    ls_fields = ls_fields && lv_str_infty && 'ENDDA' && p_sep.
    ls_fields_description = ls_fields_description && 'Dt. Início' && p_sep.

    delete lt_q588m
        where kein = abap_true
          or ausb = abap_true
          or init = abap_true.

    field-symbols <q588m> like line of lt_q588m.
    loop at lt_q588m assigning <q588m>.
      ls_fields = ls_fields && <q588m>-tname && p_sep.
      ls_fields_description = ls_fields_description && <q588m>-lname && p_sep.
    endloop.

    append ls_fields_description to lt_out.
    append ls_fields to lt_out.

    cl_gui_frontend_services=>gui_download(
      exporting
        filename                  = lv_filename
      changing
        data_tab                  = lt_out
    ).


  endmethod.                    "download_template


  method at_selection_screen_for_subty.

    call function 'HR_F4_GET_SUBTYPE'
      exporting
        infty                = p_infty
        molga                = '37'
      importing
        subty                = r_result
      exceptions
        infty_not_found      = 1
        no_entries_found     = 2
        cancelled            = 3
        infty_not_supported  = 4
        infty_has_no_subties = 5.

  endmethod.                    "at_selection_screen_for_subty

endclass.                    "lcl_carga_infty IMPLEMENTATION

data o_app type ref to lcl_carga_infty.

initialization.

  o_app = lcl_carga_infty=>get_instance( ).
  o_app->load_selscreen_texts( ).

at selection-screen.
  o_app->at_selection_screen_ucomm( sscrfields-ucomm ).

at selection-screen on value-request for p_file.

  p_file = o_app->at_selection_screen_for_file( ).

at selection-screen on value-request for p_subty.

  p_subty = o_app->at_selection_screen_for_subty( ).

start-of-selection.

  o_app->start_of_selection( ).  o_app->start_of_selection( ).
