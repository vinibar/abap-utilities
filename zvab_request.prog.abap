*--------------------------------------------------------------------*
*
*                     == Request Organizer ==
*
*--------------------------------------------------------------------*
* Developed by:
*  - Douglas Samaniego / viodoug@gmail.com
*  - Vinicius Barrionuevo (@vinibar) / vinicius21@gmail.com
*--------------------------------------------------------------------*
REPORT zvab_request.

TABLES e071.

DATA g_devclas TYPE tadir-devclass.

PARAMETERS:
  p_pgmid TYPE e071-pgmid DEFAULT 'R3TR',
  p_object TYPE e071-object.

SELECT-OPTIONS: s_dvclas FOR g_devclas
                NO INTERVALS OBLIGATORY.

***SELECTION-SCREEN SKIP 1.
***
**** Seleção Individual
***SELECTION-SCREEN BEGIN OF BLOCK b2 WITH FRAME TITLE text-f02.
***PARAMETERS: p_objnam TYPE e071-obj_name.
***SELECTION-SCREEN END OF BLOCK b2.

SELECTION-SCREEN SKIP 1.

* Seleções Multiplas
SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE text-f01.
SELECT-OPTIONS:   s_objnam FOR e071-obj_name MEMORY ID obj.
SELECTION-SCREEN END OF BLOCK b1.

CLASS lcl_requests_organizer DEFINITION DEFERRED.

DATA o_requests TYPE REF TO lcl_requests_organizer.

*----------------------------------------------------------------------*
*       CLASS lcl_report DEFINITION
*----------------------------------------------------------------------*
* - Report with all dependencies
*----------------------------------------------------------------------*
CLASS lcl_report DEFINITION.

  PUBLIC SECTION.

    TYPES:
    BEGIN OF ty_out,
      pgmid TYPE tadir-pgmid,
      object TYPE tadir-object,
      obj_name  TYPE tadir-obj_name,
      e071_obj_name TYPE e071-obj_name,
      trkorr TYPE e071-trkorr,
      as4text TYPE e07t-as4text,
* SOLUTIOIT - Douglas Samaniego - 05.04.2017 08:12:56 - Início
      devclass TYPE tadir-devclass,
* SOLUTIOIT - Douglas Samaniego - 05.04.2017 08:12:56 - Fim
    END OF ty_out.

    TYPES: ty_t_out TYPE TABLE OF ty_out WITH DEFAULT KEY.

    DATA o_alv TYPE REF TO cl_salv_table.
    DATA o_data TYPE REF TO lcl_requests_organizer.

    METHODS:
      constructor
        IMPORTING
          it_out  TYPE ty_t_out
          io_data TYPE REF TO lcl_requests_organizer.

    METHODS:
      on_added_function FOR EVENT added_function OF cl_salv_events_table
        IMPORTING e_salv_function,

      display.

  PRIVATE SECTION.
    DATA t_out TYPE ty_t_out.

    METHODS:
      assign_objects,
      unassign_objects,
      update_request,
      get_event_sm30
        IMPORTING is_ko200 TYPE ko200
          CHANGING chg_ko200 TYPE tredt_objects
                   chg_e071k TYPE tredt_keys.

ENDCLASS.                    "lcl_report DEFINITION

*----------------------------------------------------------------------*
*       CLASS lcl_main DEFINITION
*----------------------------------------------------------------------*
* - Main Class
*----------------------------------------------------------------------*
CLASS lcl_requests_organizer DEFINITION.


  PUBLIC SECTION.
    DATA:
      o_report TYPE REF TO lcl_report.

    TYPES: BEGIN OF ty_tadir.
            INCLUDE TYPE tadir.
    TYPES: e071_obj_name TYPE e071-obj_name.
    TYPES: END OF ty_tadir.

    TYPES: BEGIN OF ty_sm30,
             tabname  TYPE tvdir-tabname,
             area    TYPE tvdir-area,
       END OF ty_sm30.

    TYPES: ty_senvi TYPE TABLE OF senvi WITH DEFAULT KEY.

    DATA tg_eventos_sm30 TYPE TABLE OF tvimf.
    DATA tg_sm30 TYPE TABLE OF ty_sm30.

    DATA t_envi_tab TYPE TABLE OF senvi.
    DATA t_aux      TYPE TABLE OF senvi.
    DATA t_tadir TYPE TABLE OF ty_tadir.
    DATA t_e071 TYPE TABLE OF e071.
    DATA t_e07t TYPE TABLE OF e07t.

    METHODS:
      main,
      select_data RETURNING value(rt_out) TYPE lcl_report=>ty_t_out.

    CLASS-DATA: t_envi_tab_all TYPE TABLE OF senvi.

  PRIVATE SECTION.

*    CLASS-METHODS:
    METHODS:

      get_depend
        IMPORTING
          iv_object TYPE tadir-object
          iv_name   TYPE e071-obj_name
        EXPORTING rt_out TYPE lcl_report=>ty_t_out,

    get_sm30
       CHANGING value(ch_t_env_tab) TYPE senvi_tab.

    CLASS-METHODS:
       get_dependents
         IMPORTING
           i_obj_type TYPE euobj-id
           i_object_name TYPE tadir-obj_name
*         RETURNING value(rt_envi_tab) TYPE ty_senvi.
         CHANGING value(rt_envi_tab) TYPE ty_senvi.

ENDCLASS.                    "lcl_main DEFINITION


*----------------------------------------------------------------------*
*       CLASS LCL_REPORT IMPLEMENTATION
*----------------------------------------------------------------------*
* - Report with all dependencies
*----------------------------------------------------------------------*
CLASS lcl_report IMPLEMENTATION.

  METHOD constructor.

    me->t_out   = it_out.
    me->o_data  = io_data.

  ENDMETHOD.                    "constructor

  METHOD on_added_function.

    CASE e_salv_function.
      WHEN 'ASSIGN'.
        me->assign_objects( ).
      WHEN 'UNASSIGN'.
        me->unassign_objects( ).
    ENDCASE.

* Atualiza requests do log
    me->update_request( ).

*    me->t_out = o_requests->select_data( ).
    o_alv->refresh( ).

  ENDMETHOD.                    "on_added_function

  METHOD display.

    DATA lo_columns TYPE REF TO cl_salv_columns.
    DATA lo_column TYPE REF TO cl_salv_column_list.
    DATA lo_events TYPE REF TO cl_salv_events_table.
    DATA lo_selections TYPE REF TO cl_salv_selections.
    DATA lo_display TYPE REF TO cl_salv_display_settings.

    IF o_alv IS NOT BOUND.

      TRY.

          SORT o_requests->t_tadir BY object
                                      obj_name.

          SORT t_out BY object
                        obj_name.

          cl_salv_table=>factory(
          IMPORTING
            r_salv_table   = o_alv
          CHANGING
            t_table        = t_out
            ).
        CATCH cx_salv_msg.
          " ERRO AO GERAR ALV
      ENDTRY.

      o_alv->set_screen_status(
        pfstatus = 'SALV_STANDARD'
        report = sy-repid
        set_functions = o_alv->c_functions_all ).


      lo_events = o_alv->get_event( ).
      SET HANDLER me->on_added_function FOR lo_events.

      lo_columns = o_alv->get_columns( ).
      lo_columns->set_optimize( ).
      lo_column ?= lo_columns->get_column( 'E071_OBJ_NAME' ).
      lo_column->set_visible( abap_false ).

      lo_display = o_alv->get_display_settings( ).
      lo_display->set_striped_pattern( cl_salv_display_settings=>true )
.
      lo_display->set_list_header( 'Organizador de Requests' ).


      lo_selections = o_alv->get_selections( ).
      lo_selections->set_selection_mode(
      if_salv_c_selection_mode=>row_column
*        if_salv_c_selection_mode=>multiple
      ).

      o_alv->display( ).

    ENDIF.


  ENDMETHOD.                    "display

  METHOD assign_objects.

    DATA lt_selected_rows TYPE lvc_t_row.
    DATA lo_selections TYPE REF TO cl_salv_selections.
    DATA ls_request TYPE trwbo_request.
    DATA ls_i_ko200   TYPE ko200.
    DATA ls_i_ko200_1 TYPE ko200.
    DATA lt_ko200   TYPE tredt_objects.
    DATA lt_e071k   TYPE tredt_keys.
    DATA ls_e_ko200 LIKE ls_i_ko200.
    DATA lv_selected_request TYPE e070-trkorr.
    DATA lv_conf_req   TYPE c.
    DATA lv_title(400) TYPE c.
    DATA lv_tot_obj    TYPE i.

    FIELD-SYMBOLS <out> LIKE LINE OF t_out.
    FIELD-SYMBOLS <tadir> LIKE LINE OF lcl_requests_organizer=>t_tadir.
    FIELD-SYMBOLS <selected_row> LIKE LINE OF lt_selected_rows.

    DATA ls_e071k   LIKE LINE OF lt_e071k.
    DATA lv_tab_key TYPE trobj_name.

* Leitura das linhas selecionadas no ALV
    lo_selections = o_alv->get_selections( ).

    lt_selected_rows = lo_selections->get_selected_rows( ).

    CLEAR lt_ko200.

    DESCRIBE TABLE lt_selected_rows LINES lv_tot_obj.

    LOOP AT lt_selected_rows ASSIGNING <selected_row>.

      READ TABLE t_out ASSIGNING <out> INDEX <selected_row>-index.

      READ TABLE o_requests->t_tadir ASSIGNING <tadir>
        INDEX sy-tabix.

      IF sy-subrc IS INITIAL.
        MOVE-CORRESPONDING <tadir> TO ls_i_ko200.

        IF ls_e_ko200-trkorr IS NOT INITIAL.

          SELECT SINGLE strkorr FROM e070
            INTO lv_selected_request
            WHERE trkorr = ls_e_ko200-trkorr.

        ENDIF.

        APPEND ls_i_ko200 TO lt_ko200.

* Verifica Entradas TDDAT e TVDIR
        IF ls_i_ko200-object = 'TABL'.
          READ TABLE o_data->tg_sm30 TRANSPORTING NO FIELDS
                                     WITH KEY tabname = ls_i_ko200-obj_name.
          IF sy-subrc = 0.

* Leitura das entradas
            lv_tab_key = ls_i_ko200-obj_name.

* Monta Entradas na request
            SELECT * UP TO 1 ROWS
              FROM e071k
              INTO ls_e071k
              WHERE pgmid   = 'R3TR' AND
                    object  = 'TABU' AND
                    objname = 'TDDAT' AND
                     tabkey = lv_tab_key.
            ENDSELECT.

            IF sy-subrc = 0.
              CLEAR: ls_e071k-trkorr,
                     ls_e071k-as4pos.
              APPEND ls_e071k TO lt_e071k.

* Insere entrada na request para a leitura das chaves
              READ TABLE lt_ko200 TRANSPORTING NO FIELDS
                                 WITH KEY pgmid    = 'R3TR'
                                          object   = 'TABU'
                                          obj_name = 'TDDAT'.
              IF sy-subrc <> 0.
                ls_i_ko200_1 = ls_i_ko200.
                ls_i_ko200_1-pgmid    = 'R3TR'.
                ls_i_ko200_1-object   = 'TABU'.
                ls_i_ko200_1-obj_name = 'TDDAT'.
                ls_i_ko200_1-objfunc = 'K'.
                APPEND ls_i_ko200_1 TO lt_ko200.
              ENDIF.
            ENDIF.

            CLEAR ls_e071k.
            SELECT * UP TO 1 ROWS
              FROM e071k
              INTO ls_e071k
              WHERE pgmid   = 'R3TR' AND
                    object  = 'TABU' AND
                    objname = 'TVDIR' AND
                     tabkey = lv_tab_key.
            ENDSELECT.

            IF sy-subrc = 0.
              CLEAR: ls_e071k-trkorr,
                     ls_e071k-as4pos.
              APPEND ls_e071k TO lt_e071k.

* Insere entrada na request para a leitura das chaves
              READ TABLE lt_ko200 TRANSPORTING NO FIELDS
                                  WITH KEY pgmid    = 'R3TR'
                                           object   = 'TABU'
                                           obj_name = 'TVDIR'.
              IF sy-subrc <> 0.
                ls_i_ko200_1 = ls_i_ko200.
                ls_i_ko200_1-pgmid    = 'R3TR'.
                ls_i_ko200_1-object   = 'TABU'.
                ls_i_ko200_1-obj_name = 'TVDIR'.
                ls_i_ko200_1-objfunc  = 'K'.
                APPEND ls_i_ko200_1 TO lt_ko200.
              ENDIF.
            ENDIF.

          ENDIF.
        ENDIF.
* Caso o objeto a ser adicionado seja uma tabela, verifica se possui
* eventos de horas e adiciona os objetos
        me->get_event_sm30( EXPORTING is_ko200  = ls_i_ko200
                             CHANGING chg_ko200 = lt_ko200
                                      chg_e071k = lt_e071k ).
        CLEAR ls_i_ko200.


* Altera Função para contemplar entradas de tabela
        CALL FUNCTION 'TRINT_OBJECTS_CHECK_AND_INSERT'
          EXPORTING
            iv_order                       = lv_selected_request
            iv_with_dialog                 = 'X'
            iv_send_message                = 'X'
            iv_old_call                    = 'X'
          CHANGING
            ct_ko200                       = lt_ko200
            ct_e071k                       = lt_e071k
*           CT_E071K_STR                   =
          EXCEPTIONS
            cancel_edit_append_error_keys  = 1
            cancel_edit_append_error_objct = 2
            cancel_edit_append_error_order = 3
            cancel_edit_but_se01           = 4
            cancel_edit_no_header_object   = 5
            cancel_edit_no_order_selected  = 6
            cancel_edit_repaired_object    = 7
            cancel_edit_system_error       = 8
            cancel_edit_tadir_missing      = 9
            cancel_edit_tadir_update_error = 10
            cancel_edit_unknown_devclass   = 11
            cancel_edit_unknown_objecttype = 12
            show_only_closed_system        = 13
            show_only_consolidation_level  = 14
            show_only_ddic_in_customer_sys = 15
            show_only_delivery_system      = 16
            show_only_different_ordertypes = 17
            show_only_different_tasktypes  = 18
            show_only_enqueue_failed       = 19
            show_only_generated_object     = 20
            show_only_ill_lock             = 21
            show_only_lock_enqueue_failed  = 22
            show_only_mixed_orders         = 23
            show_only_mix_local_transp_obj = 24
            show_only_no_shared_repair     = 25
            show_only_object_locked        = 26
            show_only_repaired_object      = 27
            show_only_show_client          = 28
            show_only_tadir_missing        = 29
            show_only_unknown_devclass     = 30
            cancel_edit_no_check_call      = 31
            cancel_edit_category_mixture   = 32
            show_only_closed_client        = 33
            show_only_closed_ale_object    = 34
            show_only_unallowed_superuser  = 35
            cancel_edit_custom_obj_at_sap  = 36
            cancel_edit_access_denied      = 37
            show_only_no_repair_system     = 38
            show_only_no_license           = 39
            show_only_central_basis        = 40
            show_only_user_after_error     = 41
            cancel_edit_user_after_error   = 42
            show_only_object_not_patchable = 43
            OTHERS                         = 44.
        IF sy-subrc <> 0.
* Implement suitable error handling here
        ENDIF.

* Douglas Samaniego - 16.11.2017 07:34:30 - Início
* Após incluir o objeto a primeira request, verifica se os demais objetos
* caso existam > 1, serão adicionados na mesma request evitando confirmar
* a mesma request varias vezes
        IF lv_conf_req IS INITIAL
          AND lv_tot_obj > 1.

          READ TABLE lt_ko200 INTO ls_i_ko200_1 INDEX 1.

          CONCATENATE 'GOSTARIA DE ADICIONAR OS OBJETOS A REQUEST'
                       ls_i_ko200_1-trkorr INTO lv_title SEPARATED BY space.

          CALL FUNCTION 'POPUP_TO_CONFIRM'
            EXPORTING
              titlebar                    = 'ADICIONAR OBJETOS A REQUEST'
*       DIAGNOSE_OBJECT             = ' '
              text_question               = lv_title
*       TEXT_BUTTON_1               = 'Ja'(001)
*       ICON_BUTTON_1               = ' '
*       TEXT_BUTTON_2               = 'Nein'(002)
*       ICON_BUTTON_2               = ' '
*       DEFAULT_BUTTON              = '1'
              display_cancel_button       = ' '
*       USERDEFINED_F1_HELP         = ' '
*       START_COLUMN                = 25
*       START_ROW                   = 6
*       POPUP_TYPE                  =
*       IV_QUICKINFO_BUTTON_1       = ' '
*       IV_QUICKINFO_BUTTON_2       = ' '
           IMPORTING
             answer                      = lv_conf_req
*     TABLES
*       PARAMETER                   =
           EXCEPTIONS
             text_not_found              = 1
             OTHERS                      = 2
                    .
          IF sy-subrc <> 0.
* Implement suitable error handling here
          ENDIF.

          IF lv_conf_req = '1'.
            lv_selected_request = ls_i_ko200_1-trkorr.
          ENDIF.

          CLEAR ls_i_ko200_1.
        ENDIF.
* Douglas Samaniego - 16.11.2017 07:34:30 - Fim

        CLEAR lt_ko200.

      ENDIF.

    ENDLOOP.

    IF lt_ko200 IS NOT INITIAL.

** Altera Função para contemplar entradas de tabela
*      call function 'TRINT_OBJECTS_CHECK_AND_INSERT'
*        exporting
*          iv_order                       = lv_selected_request
*          iv_with_dialog                 = 'X'
*          iv_send_message                = 'X'
*          iv_old_call                    = 'X'
*        changing
*          ct_ko200                       = lt_ko200
*          ct_e071k                       = lt_e071k
**         CT_E071K_STR                   =
*        exceptions
*          cancel_edit_append_error_keys  = 1
*          cancel_edit_append_error_objct = 2
*          cancel_edit_append_error_order = 3
*          cancel_edit_but_se01           = 4
*          cancel_edit_no_header_object   = 5
*          cancel_edit_no_order_selected  = 6
*          cancel_edit_repaired_object    = 7
*          cancel_edit_system_error       = 8
*          cancel_edit_tadir_missing      = 9
*          cancel_edit_tadir_update_error = 10
*          cancel_edit_unknown_devclass   = 11
*          cancel_edit_unknown_objecttype = 12
*          show_only_closed_system        = 13
*          show_only_consolidation_level  = 14
*          show_only_ddic_in_customer_sys = 15
*          show_only_delivery_system      = 16
*          show_only_different_ordertypes = 17
*          show_only_different_tasktypes  = 18
*          show_only_enqueue_failed       = 19
*          show_only_generated_object     = 20
*          show_only_ill_lock             = 21
*          show_only_lock_enqueue_failed  = 22
*          show_only_mixed_orders         = 23
*          show_only_mix_local_transp_obj = 24
*          show_only_no_shared_repair     = 25
*          show_only_object_locked        = 26
*          show_only_repaired_object      = 27
*          show_only_show_client          = 28
*          show_only_tadir_missing        = 29
*          show_only_unknown_devclass     = 30
*          cancel_edit_no_check_call      = 31
*          cancel_edit_category_mixture   = 32
*          show_only_closed_client        = 33
*          show_only_closed_ale_object    = 34
*          show_only_unallowed_superuser  = 35
*          cancel_edit_custom_obj_at_sap  = 36
*          cancel_edit_access_denied      = 37
*          show_only_no_repair_system     = 38
*          show_only_no_license           = 39
*          show_only_central_basis        = 40
*          show_only_user_after_error     = 41
*          cancel_edit_user_after_error   = 42
*          show_only_object_not_patchable = 43
*          others                         = 44.
*      if sy-subrc <> 0.
** Implement suitable error handling here
*      endif.

      FIELD-SYMBOLS <ko200> LIKE LINE OF lt_ko200.
      LOOP AT lt_ko200 ASSIGNING <ko200>.
        READ TABLE t_out ASSIGNING <out>
            WITH KEY object = <ko200>-object
                     obj_name = <ko200>-obj_name.

        IF sy-subrc IS INITIAL.
          <out>-trkorr = <ko200>-trkorr.
          SELECT SINGLE as4text FROM e07t
            INTO <out>-as4text
            WHERE trkorr = <out>-trkorr.
        ENDIF.
      ENDLOOP.

    ENDIF.

  ENDMETHOD.                    "assign_objects

  METHOD unassign_objects.

    DATA lt_selected_rows TYPE lvc_t_row.
    DATA lo_selections TYPE REF TO cl_salv_selections.
    DATA lt_locks TYPE trwbo_t_locktab.
    DATA ls_request TYPE trwbo_request.
    FIELD-SYMBOLS <out> LIKE LINE OF t_out.
    FIELD-SYMBOLS <e071> LIKE LINE OF lcl_requests_organizer=>t_e071.
    FIELD-SYMBOLS <selected_row> LIKE LINE OF lt_selected_rows.

    lo_selections = o_alv->get_selections( ).

    lt_selected_rows = lo_selections->get_selected_rows( ).

    LOOP AT lt_selected_rows ASSIGNING <selected_row>.

      READ TABLE t_out ASSIGNING <out> INDEX <selected_row>-index.

      READ TABLE o_requests->t_e071 ASSIGNING <e071>
        WITH KEY pgmid = <out>-pgmid
                 object = <out>-object
                 obj_name = <out>-obj_name.

      IF sy-subrc IS INITIAL.

        ls_request-h-trkorr = <e071>-trkorr.

        CALL FUNCTION 'TRINT_SORT_AND_COMPRESS_COMM'
          EXPORTING
            iv_trkorr            = <e071>-trkorr
          EXCEPTIONS
            request_doesnt_exist = 1
            request_released     = 2
            no_authorization     = 3
            update_error         = 4
            OTHERS               = 5.

        CALL FUNCTION 'TR_DELETE_COMM_OBJECT_KEYS'
          EXPORTING
            is_e071_delete              = <e071>
          IMPORTING
            et_new_locks                = lt_locks
          CHANGING
            cs_request                  = ls_request
          EXCEPTIONS
            e_database_access_error     = 1
            e_empty_lockkey             = 2
            e_bad_target_request        = 3
            e_wrong_source_client       = 4
            n_no_deletion_of_c_objects  = 5
            n_no_deletion_of_corr_entry = 6
            n_object_entry_doesnt_exist = 7
            n_request_already_released  = 8
            n_request_from_other_system = 9
            r_action_aborted_by_user    = 10
            r_foreign_lock              = 11
            w_bigger_lock_in_same_order = 12
            w_duplicate_entry           = 13
            w_no_authorization          = 14
            w_user_not_owner            = 15
            OTHERS                      = 16.
        IF sy-subrc <> 0.
* Implement suitable error handling here
          MESSAGE ID sy-msgid TYPE sy-msgty NUMBER sy-msgno
                  WITH sy-msgv1 sy-msgv2 sy-msgv3 sy-msgv4.
        ENDIF.

* Atualiza log de request
        IF sy-subrc = 0.
          CLEAR: <out>-trkorr,
                 <out>-as4text.
        ENDIF.

      ENDIF.


    ENDLOOP.



  ENDMETHOD.                    "unassign_objects

  METHOD update_request.

    TYPES: BEGIN OF ty_tadir.
            INCLUDE TYPE tadir.
    TYPES: e071_obj_name TYPE e071-obj_name.
    TYPES: END OF ty_tadir.

    DATA: lt_out_aux TYPE ty_t_out,
          lt_tadir   TYPE TABLE OF ty_tadir,
          lv_tabix   TYPE sy-tabix.

    DATA lt_e070 TYPE TABLE OF e070.
    DATA lt_e071 TYPE TABLE OF e071.
    DATA lt_e07t TYPE TABLE OF e07t.

    FIELD-SYMBOLS: <tadir> TYPE ty_tadir,
                   <out>   TYPE ty_out,
                   <e070>  TYPE e070,
                   <e071>  TYPE e071,
                   <e07t>  TYPE e07t.

**********************************************************************
    lt_out_aux = t_out.
    SORT lt_out_aux BY trkorr.

**    DELETE lt_out_aux WHERE trkorr IS NOT INITIAL.

    IF lt_out_aux IS NOT INITIAL.

      SELECT * FROM tadir
      INTO TABLE lt_tadir
      FOR ALL ENTRIES IN lt_out_aux
      WHERE object = lt_out_aux-object(4)
        AND obj_name = lt_out_aux-obj_name(40)
        AND devclass IN s_dvclas.

      LOOP AT lt_tadir ASSIGNING <tadir>.
        <tadir>-e071_obj_name = <tadir>-obj_name.
      ENDLOOP.

      IF lt_tadir IS NOT INITIAL.

        SELECT * FROM e071
        INTO TABLE lt_e071
        FOR ALL ENTRIES IN lt_tadir
        WHERE pgmid = lt_tadir-pgmid
          AND object = lt_tadir-object
          AND obj_name = lt_tadir-e071_obj_name.

        IF sy-subrc IS INITIAL.
          SELECT * FROM e07t
          INTO TABLE lt_e07t
          FOR ALL ENTRIES IN lt_e071
          WHERE trkorr = lt_e071-trkorr
          AND langu = 'PT'.

          IF lt_e07t IS NOT INITIAL.
            SELECT * FROM e070
              INTO TABLE lt_e070
              FOR ALL ENTRIES IN lt_e07t
              WHERE trkorr = lt_e07t-trkorr
                AND trstatus = 'D'.
          ENDIF.
        ENDIF.

        LOOP AT lt_e071 ASSIGNING <e071>.

          lv_tabix = sy-tabix.

          READ TABLE lt_e070 TRANSPORTING NO FIELDS
            WITH KEY trkorr = <e071>-trkorr.

          IF sy-subrc IS NOT INITIAL.
            DELETE lt_e071 INDEX lv_tabix.
          ENDIF.
        ENDLOOP.

        IF lt_e071 IS INITIAL.
          RETURN.
        ENDIF.

* Atualiza requests
        LOOP AT lt_tadir ASSIGNING <tadir>.
          READ TABLE t_out ASSIGNING <out>
                           WITH KEY object  = <tadir>-object
                                    obj_name = <tadir>-obj_name.
          IF sy-subrc = 0.

            READ TABLE lt_e071 ASSIGNING <e071>
                               WITH KEY pgmid    = <tadir>-pgmid
                                        object   = <tadir>-object
                                        obj_name = <tadir>-e071_obj_name.
            IF sy-subrc = 0.

              READ TABLE lt_e070 TRANSPORTING NO FIELDS
                                  WITH KEY trkorr = <e071>-trkorr.

              IF sy-subrc = 0.
                <out>-trkorr = <e071>-trkorr.

                READ TABLE lt_e07t ASSIGNING <e07t>
                                   WITH KEY trkorr = <e071>-trkorr.
                IF sy-subrc = 0.
                  <out>-as4text = <e07t>-as4text.
                ENDIF.

              ENDIF.
            ENDIF.
          ENDIF.
        ENDLOOP.

      ENDIF.
    ENDIF.

  ENDMETHOD.                    "update_request

  METHOD get_event_sm30.

    DATA ls_e071k   LIKE LINE OF chg_e071k.
    DATA ls_i_ko200 TYPE ko200.
    DATA lv_tab_key TYPE trobj_name.

    FIELD-SYMBOLS <fs_eventos>   LIKE LINE OF o_data->tg_eventos_sm30.


    ls_i_ko200 = is_ko200.

    IF is_ko200-object = 'TABL'.
      READ TABLE o_data->tg_eventos_sm30 TRANSPORTING NO FIELDS
                                          WITH KEY tabname = is_ko200-obj_name
                                          BINARY SEARCH.
      IF sy-subrc = 0.
        LOOP AT o_data->tg_eventos_sm30 ASSIGNING <fs_eventos> FROM sy-tabix.
          IF <fs_eventos>-tabname <> is_ko200-obj_name.
            EXIT.
          ENDIF.

* Leitura das entradas
          lv_tab_key = <fs_eventos>-tabname.
          lv_tab_key+30 = <fs_eventos>-event.
* Monta Entradas na request
          SELECT * UP TO 1 ROWS
            FROM e071k
            INTO ls_e071k
            WHERE pgmid   = 'R3TR' AND
                  object  = 'TABU' AND
                  objname = 'TVIMF' AND
                  mastertype = 'VDAT' AND
                   tabkey = lv_tab_key.
          ENDSELECT.

          IF sy-subrc = 0.
            CLEAR ls_e071k-trkorr.
            APPEND ls_e071k TO chg_e071k.


* Insere entrada na request para a leitura das chaves
            ls_i_ko200-pgmid    = 'R3TR'.
            ls_i_ko200-object   = 'VDAT'.
            ls_i_ko200-obj_name = 'V_TVIMF'.
            ls_i_ko200-objfunc = 'K'.
            APPEND ls_i_ko200 TO chg_ko200.
          ENDIF.

* Monta Entradas na request
          SELECT * UP TO 1 ROWS
            FROM e071k
            INTO ls_e071k
            WHERE pgmid   = 'R3TR' AND
                  object  = 'TABU' AND
                  objname = 'TVIMF' AND
                  mastertype = 'TABU' AND
                   tabkey = lv_tab_key.
          ENDSELECT.

          IF sy-subrc = 0.
            CLEAR ls_e071k-trkorr.
            APPEND ls_e071k TO chg_e071k.
* Insere entrada na request para a leitura das chaves
            ls_i_ko200 = ls_i_ko200.
            ls_i_ko200-pgmid    = 'R3TR'.
            ls_i_ko200-object   = 'TABU'.
            ls_i_ko200-obj_name = 'TVIMF'.
            ls_i_ko200-objfunc = 'K'.
            APPEND ls_i_ko200 TO chg_ko200.
          ENDIF.

        ENDLOOP.
      ENDIF.
    ENDIF.

  ENDMETHOD.                    "get_event_sm30

ENDCLASS.                    "LCL_REPORT IMPLEMENTATION

*----------------------------------------------------------------------*
*       CLASS lcl_main IMPLEMENTATION
*----------------------------------------------------------------------*
* - Main Class
*----------------------------------------------------------------------*
CLASS lcl_requests_organizer IMPLEMENTATION.

  METHOD main.

    CREATE OBJECT o_report
      EXPORTING
        it_out  = o_requests->select_data( )
        io_data = o_requests.

    o_report->display( ).

  ENDMETHOD.                    "main

  METHOD select_data.

    TYPES: BEGIN OF ty_tadir,
            pgmid    TYPE tadir-pgmid   ,
            object   TYPE tadir-object  ,
            obj_name TYPE tadir-obj_name,
           END OF ty_tadir.

    DATA tl_tadir TYPE STANDARD TABLE OF ty_tadir.

    FIELD-SYMBOLS <fs_tadir> TYPE ty_tadir.

    DATA lv_name TYPE e071-obj_name.

* Limpa variaveis globais para reprocessamento
    CLEAR: t_envi_tab_all, t_envi_tab, tg_sm30,
           t_aux, t_e07t, t_tadir.

* Seleção de multiplos objetos
    IF s_objnam[] IS NOT INITIAL.

      SELECT pgmid
             object
             obj_name
        FROM tadir
        INTO TABLE tl_tadir
      WHERE pgmid = p_pgmid
      AND object  = p_object
      AND obj_name  IN s_objnam
      AND devclass IN s_dvclas.

      IF tl_tadir IS INITIAL.
        MESSAGE 'Objeto inválido' TYPE 'E'.
        RETURN.
      ENDIF.

* Carrega os objetos dependentes
      LOOP AT tl_tadir ASSIGNING <fs_tadir>.

* Busca dependencias
        CLEAR: lv_name, rt_out.
        lv_name = <fs_tadir>-obj_name.
        me->get_depend( EXPORTING iv_object = <fs_tadir>-object
                                  iv_name   = lv_name
                        IMPORTING rt_out = rt_out ).


      ENDLOOP.

**    ELSE.
**
**      SELECT COUNT(*) FROM tadir
**      WHERE pgmid  = p_pgmid
**      AND object   = p_object
**      AND obj_name = p_objnam.
**
**      IF sy-subrc IS NOT INITIAL.
**        MESSAGE 'Objeto inválido' TYPE 'E'.
**      ENDIF.
**
*** Busca dependencias
**      CLEAR: lv_name, rt_out.
**      me->get_depend( EXPORTING iv_object = p_object
**                                iv_name   = p_objnam
**                      IMPORTING rt_out = rt_out ).


    ENDIF.

  ENDMETHOD.                    "select_data

  METHOD get_depend.

    DATA lt_e070 TYPE TABLE OF e070.
    DATA tl_obj_bloq TYPE TABLE OF dd25l.
    DATA tl_catg     TYPE TABLE OF dd40l.
    DATA tl_trans    TYPE TABLE OF tstc.
    DATA tl_menu     TYPE TABLE OF rsmptexts.

    DATA lv_obj_type    TYPE euobj-id.
    DATA lv_object_name TYPE tadir-obj_name.
    DATA lv_tabix       TYPE sy-tabix.

    FIELD-SYMBOLS <tadir> LIKE LINE OF t_tadir.
    FIELD-SYMBOLS <e071> LIKE LINE OF t_e071.
    FIELD-SYMBOLS <e07t> LIKE LINE OF t_e07t.
    FIELD-SYMBOLS <fs_obj_bloq> TYPE dd25l.
    FIELD-SYMBOLS <fs_ctag> TYPE dd40l.
    FIELD-SYMBOLS <fs_trans> TYPE tstc.
    FIELD-SYMBOLS <fs_menu> TYPE rsmptexts.
    FIELD-SYMBOLS <out> LIKE LINE OF rt_out.
    FIELD-SYMBOLS <fs_envi_tab> LIKE LINE OF t_envi_tab.

    lv_obj_type = iv_object.
    lv_object_name = iv_name.

**    t_envi_tab = o_requests->get_dependents(
**      i_obj_type = lv_obj_type
**      i_object_name = lv_object_name ).

    o_requests->get_dependents( EXPORTING
      i_obj_type = lv_obj_type
      i_object_name = lv_object_name
      CHANGING rt_envi_tab = t_envi_tab ).

    IF t_envi_tab IS NOT INITIAL.

      t_aux = t_envi_tab.
      SORT t_aux BY type.
      DELETE t_aux WHERE type <> 'TABL'
                     AND type <> 'VIEW'.

      IF t_aux IS NOT INITIAL.

* Para os objetos do tipo tabela verifica se existe SM30
        me->get_sm30( CHANGING ch_t_env_tab = t_envi_tab ).

* Lista Objetos de Bloqueio das Tabelas
        SELECT * FROM dd25l
          INTO TABLE tl_obj_bloq
          FOR ALL ENTRIES IN t_aux
          WHERE roottab = t_aux-object(30).

        IF sy-subrc = 0.

* Adiciona objeto na lista
          LOOP AT tl_obj_bloq ASSIGNING <fs_obj_bloq>.
            APPEND INITIAL LINE TO t_envi_tab ASSIGNING <fs_envi_tab>.
            <fs_envi_tab>-type   = 'ENQU'.
            <fs_envi_tab>-object = <fs_obj_bloq>-viewname.
          ENDLOOP.
        ENDIF.

* Lista Categoria de Tabelas
        SELECT * FROM dd40l
          INTO TABLE tl_catg
          FOR ALL ENTRIES IN t_aux
          WHERE rowtype = t_aux-object(30).

        IF sy-subrc = 0.

* Adiciona objeto na lista
          LOOP AT tl_catg ASSIGNING <fs_ctag>.
            APPEND INITIAL LINE TO t_envi_tab ASSIGNING <fs_envi_tab>.
            <fs_envi_tab>-type   = 'TTYP'.
            <fs_envi_tab>-object = <fs_ctag>-typename.
          ENDLOOP.
        ENDIF.
      ENDIF.

* Busca Transações para os programas
      SELECT * FROM tstc
        INTO TABLE tl_trans
        FOR ALL ENTRIES IN t_envi_tab
        WHERE pgmna = t_envi_tab-object(40).

      IF sy-subrc = 0.
* Adiciona objeto na lista
        LOOP AT tl_trans ASSIGNING <fs_trans>.
          APPEND INITIAL LINE TO t_envi_tab ASSIGNING <fs_envi_tab>.
          <fs_envi_tab>-type   = 'TRAN'.
          <fs_envi_tab>-object = <fs_trans>-tcode.
        ENDLOOP.
      ENDIF.

* Busca Request dos objetos
      SELECT * FROM tadir
      INTO TABLE t_tadir
      FOR ALL ENTRIES IN t_envi_tab
      WHERE object = t_envi_tab-type(4)
        AND obj_name = t_envi_tab-object(40)
        AND devclass IN s_dvclas.

      LOOP AT t_tadir ASSIGNING <tadir>.
        <tadir>-e071_obj_name = <tadir>-obj_name.
      ENDLOOP.


      IF t_tadir IS NOT INITIAL.

        SELECT * FROM e071
        INTO TABLE t_e071
        FOR ALL ENTRIES IN t_tadir
        WHERE pgmid = t_tadir-pgmid
          AND object = t_tadir-object
          AND obj_name = t_tadir-e071_obj_name.

        IF sy-subrc IS INITIAL.
          SELECT * FROM e07t
          INTO TABLE t_e07t
          FOR ALL ENTRIES IN t_e071
          WHERE trkorr = t_e071-trkorr
          AND langu = 'PT'.

          IF t_e07t IS NOT INITIAL.
            SELECT * FROM e070
              INTO TABLE lt_e070
              FOR ALL ENTRIES IN t_e07t
              WHERE trkorr = t_e07t-trkorr
                AND trstatus = 'D'.
          ENDIF.
        ENDIF.
      ENDIF.
    ENDIF.

    LOOP AT t_e071 ASSIGNING <e071>.

      lv_tabix = sy-tabix.

      READ TABLE lt_e070 TRANSPORTING NO FIELDS
        WITH KEY trkorr = <e071>-trkorr.

      IF sy-subrc IS NOT INITIAL.
        DELETE t_e071 INDEX lv_tabix.
      ELSE.
* Adiciona objetos nao listados, encontrados nas requests
        READ TABLE t_tadir TRANSPORTING NO FIELDS
          WITH KEY pgmid    = <e071>-pgmid
                   object   = <e071>-object
                   obj_name = <e071>-obj_name.
        IF sy-subrc <> 0.
          APPEND INITIAL LINE TO t_tadir ASSIGNING <tadir>.
          MOVE-CORRESPONDING <e071> TO <tadir>.
          <tadir>-obj_name      = <e071>-obj_name(40).
          <tadir>-e071_obj_name = <e071>-obj_name.
        ENDIF.
      ENDIF.

    ENDLOOP.

    SORT t_e071 BY pgmid object obj_name.
    SORT t_e07t BY trkorr.

    LOOP AT t_tadir ASSIGNING <tadir>.

      APPEND INITIAL LINE TO rt_out ASSIGNING <out>.
      MOVE-CORRESPONDING <tadir> TO <out>.

      READ TABLE t_e071 ASSIGNING <e071>
        WITH KEY pgmid = <tadir>-pgmid
         object = <tadir>-object
         obj_name = <tadir>-e071_obj_name
         BINARY SEARCH.

      IF sy-subrc IS INITIAL.

        <out>-trkorr = <e071>-trkorr.

        READ TABLE t_e07t ASSIGNING <e07t>
        WITH KEY trkorr = <e071>-trkorr
        BINARY SEARCH.

        IF sy-subrc IS INITIAL.
          <out>-as4text = <e07t>-as4text.
        ENDIF.


      ENDIF.

    ENDLOOP.

  ENDMETHOD.                    "get_depend

* * Para os objetos do tipo tabela verifica se existe SM30
  METHOD get_sm30.

    DATA tl_sm30      TYPE TABLE OF ty_sm30.
    DATA tl_tran_sm30 TYPE TABLE OF tstcp.
    DATA wa_sm30      TYPE ty_sm30.
    DATA t_aux        TYPE TABLE OF senvi.
    DATA where_param  TYPE string.
    DATA lv_index       TYPE sy-tabix.
    DATA lv_obj_type    TYPE euobj-id.
    DATA lv_object_name TYPE tadir-obj_name.
    DATA wa_envi_tab TYPE senvi.

    FIELD-SYMBOLS <fs_envi_tab>  LIKE LINE OF t_envi_tab.
    FIELD-SYMBOLS <fs_sm30>      TYPE ty_sm30.
    FIELD-SYMBOLS <fs_tran_sm30> TYPE tstcp.

    t_aux = ch_t_env_tab.
    SORT t_aux BY type.
    DELETE t_aux WHERE type <> 'TABL'
                   AND type <> 'VIEW'.

    IF t_aux IS NOT INITIAL.

      SELECT tabname area FROM tvdir
        INTO TABLE tl_sm30
        FOR ALL ENTRIES IN t_aux
        WHERE tabname  = t_aux-object(30)
          AND devclass IN s_dvclas.

* Caso encontrado insere entradas para controle do objetos
      IF sy-subrc = 0.

* Verifica se as SM30 já foram processadas
        LOOP AT tl_sm30 ASSIGNING <fs_sm30>.
          lv_index = sy-tabix.
          READ TABLE tg_sm30 TRANSPORTING NO FIELDS
                             WITH KEY tabname = <fs_sm30>-tabname.
          IF sy-subrc = 0.
            DELETE tl_sm30 INDEX lv_index.
          ENDIF.
        ENDLOOP.

        APPEND LINES OF tl_sm30 TO tg_sm30.

        LOOP AT tl_sm30 INTO wa_sm30.
          APPEND INITIAL LINE TO ch_t_env_tab ASSIGNING <fs_envi_tab>.
* SM30
          <fs_envi_tab>-type = 'TOBJ'.
          CONCATENATE wa_sm30-tabname 'S' INTO <fs_envi_tab>-object.
          <fs_envi_tab>-call_obj  = wa_sm30-tabname.
          <fs_envi_tab>-call_type = 'TABL'.

          MOVE-CORRESPONDING <fs_envi_tab> TO wa_envi_tab.
          APPEND wa_envi_tab TO t_envi_tab_all.
          CLEAR wa_envi_tab.

          READ TABLE ch_t_env_tab TRANSPORTING NO FIELDS
                                 WITH KEY type = 'FUGR'
                                        object = wa_sm30-area.
          IF sy-subrc <> 0.

            APPEND INITIAL LINE TO ch_t_env_tab ASSIGNING <fs_envi_tab>.
* Grupo de Função
            <fs_envi_tab>-type   = 'FUGR'.
            <fs_envi_tab>-object = wa_sm30-area.
            <fs_envi_tab>-call_obj  = wa_sm30-area.
            <fs_envi_tab>-call_type = 'FUGR'.


* Para o grupo de função encontrado, lista os objetos dependentes
            CLEAR: lv_obj_type, lv_object_name.
            lv_obj_type = <fs_envi_tab>-type.
            lv_object_name = <fs_envi_tab>-object.

**              t_envi_tab = o_requests->get_dependents(
**                            i_obj_type    = lv_obj_type
**                            i_object_name = lv_object_name ).

            o_requests->get_dependents( EXPORTING
              i_obj_type = lv_obj_type
              i_object_name = lv_object_name
              CHANGING rt_envi_tab = ch_t_env_tab ).

* Para os objetos do tipo tabela verifica se existe SM30
            me->get_sm30( CHANGING ch_t_env_tab = ch_t_env_tab ).


          ENDIF.


* Lista transações para SM30
* Monta Range de sm30
          CONCATENATE 'PARAM' 'LIKE' '*%' INTO where_param SEPARATED BY space.
          CONCATENATE  where_param  wa_sm30-tabname '%*' INTO where_param.
          REPLACE ALL OCCURRENCES OF '*' IN where_param WITH ''''.

          SELECT * FROM tstcp
            INTO TABLE tl_tran_sm30
           WHERE (where_param).

          IF sy-subrc = 0.
            LOOP AT tl_tran_sm30 ASSIGNING <fs_tran_sm30>.
              APPEND INITIAL LINE TO ch_t_env_tab ASSIGNING <fs_envi_tab>.
              <fs_envi_tab>-type   = 'TRAN'.
              <fs_envi_tab>-object = <fs_tran_sm30>-tcode.
            ENDLOOP.
          ENDIF.

          CLEAR: where_param, tl_tran_sm30.


        ENDLOOP.


* Para as SM30 encontradas verifica se possui o evento de Horas
* E carrega as entradas na tabela.
        SELECT * FROM tvimf
          INTO TABLE tg_eventos_sm30
          FOR ALL ENTRIES IN tl_sm30
          WHERE tabname = tl_sm30-tabname.

        IF sy-subrc = 0.
          SORT tg_eventos_sm30 BY tabname.
        ENDIF.
      ENDIF.

    ENDIF.



  ENDMETHOD.                    "get_sm30

  METHOD get_dependents.

    DATA lt_envi_tab LIKE t_envi_tab_all.
    DATA lv_object_type TYPE euobj-id.
    DATA lv_object_name TYPE tadir-obj_name.
    DATA lt_tadir TYPE TABLE OF tadir.
    DATA lv_name_dbl  TYPE rsldb-ldb.
    DATA lv_prog_dbl  TYPE sy-ldbpg.
    DATA: lv_call_type   TYPE seu_obj,
          lv_call_object TYPE seu_objkey.

    DATA wa_envi_tab TYPE senvi.

    FIELD-SYMBOLS <envi_tab> LIKE LINE OF lt_envi_tab.
    FIELD-SYMBOLS <tadir> LIKE LINE OF lt_tadir.

    CALL FUNCTION 'REPOSITORY_ENVIRONMENT_RFC'
      EXPORTING
        obj_type        = i_obj_type
        object_name     = i_object_name
      TABLES
        environment_tab = lt_envi_tab.

    " Includes are programs so we must replace types
    LOOP AT lt_envi_tab ASSIGNING <envi_tab>.
      REPLACE 'INCL' IN <envi_tab>-type WITH 'PROG'.
      REPLACE 'STRU' IN <envi_tab>-type WITH 'TABL'.
    ENDLOOP.

* SOLUTIOIT - Douglas Samaniego - 25.04.2017 11:43:59 - Início
* Verifica se o objeto pesquisado esta sendo chamado por alguem
    READ TABLE lt_envi_tab ASSIGNING <envi_tab>
              WITH KEY type  = i_obj_type
                      object = i_object_name.
    IF sy-subrc = 0.
      IF <envi_tab>-call_obj IS INITIAL
        AND <envi_tab>-call_type IS INITIAL.
* Atualiza com ele mesmo
        <envi_tab>-call_obj  = i_object_name.
        <envi_tab>-call_type = i_obj_type.
      ENDIF.
    ENDIF.
* SOLUTIOIT - Douglas Samaniego - 25.04.2017 11:43:59 - Fim

    APPEND LINES OF lt_envi_tab TO t_envi_tab_all.


* Verifica se o objeto referido foi listado
* Caso nao anexa a lista de objetos encontrados
    READ TABLE lt_envi_tab TRANSPORTING NO FIELDS
                           WITH KEY type   = i_obj_type
                                    object = i_object_name.
    IF sy-subrc <> 0.
      wa_envi_tab-type   = i_obj_type.
      wa_envi_tab-object = i_object_name.
      APPEND wa_envi_tab TO t_envi_tab_all.
      CLEAR wa_envi_tab.
    ELSE.
      SORT lt_envi_tab BY object call_obj.
      DELETE ADJACENT DUPLICATES FROM lt_envi_tab COMPARING object call_obj.

* SOLUTIOIT - Douglas Samaniego - 25.04.2017 11:59:48 - Início
* Deleta o proprio objeto para nao ser re-listado os objetos ja carregados
*      DELETE lt_envi_tab INDEX 1. " first line is always the caller object
      READ TABLE lt_envi_tab TRANSPORTING NO FIELDS
                WITH KEY type  = i_obj_type
                        object = i_object_name.
      IF sy-subrc = 0.
        DELETE lt_envi_tab INDEX sy-tabix.
      ENDIF.
* SOLUTIOIT - Douglas Samaniego - 25.04.2017 11:59:51 - Fim
    ENDIF.

    IF lt_envi_tab IS NOT INITIAL.

* Carrega objetos Filhos
      SORT lt_envi_tab BY type object.

      SELECT * FROM tadir
        INTO TABLE lt_tadir
          FOR ALL ENTRIES IN lt_envi_tab
          WHERE object = lt_envi_tab-type(4)
            AND obj_name = lt_envi_tab-object(40)
            AND devclass IN s_dvclas. "'YPCKHR_E_SOCIAL'.

      LOOP AT lt_tadir ASSIGNING <tadir>.

        READ TABLE lt_envi_tab ASSIGNING <envi_tab>
          WITH KEY type    = <tadir>-object
                   object  = <tadir>-obj_name.
***                      binary search.

        IF sy-subrc IS NOT INITIAL.
          CONTINUE.
        ENDIF.

* Verifica se o objeto já foi processado
        READ TABLE rt_envi_tab TRANSPORTING NO FIELDS
          WITH KEY call_type = <tadir>-object
                   call_obj  = <tadir>-obj_name.
***                      binary search.

        IF sy-subrc IS INITIAL.
          CONTINUE.
        ENDIF.

* Tratativa para banco de dados logico, buscaremos as informacoes
* com base no probrama principal
        IF <envi_tab>-type = 'LDBA'.

          CLEAR: lv_name_dbl, lv_prog_dbl.

          lv_name_dbl = <envi_tab>-object.
* Busca programa principal
* Build Name of Includes :
          CALL FUNCTION 'LDB_CONVERT_LDBNAME_2_DBPROG'
            EXPORTING
              ldb_name                  = lv_name_dbl
              flag_existence_check      = space
            IMPORTING
              db_name                   = lv_prog_dbl
            EXCEPTIONS
              wrong_position_of_slashes = 1
              ldb_name_too_long         = 2
              OTHERS                    = 3.

* Check, if name is valid:
          IF sy-subrc NE 0.
            CONTINUE.
          ENDIF.

* Parametros para buscar os objetos dependentes
          lv_object_type = 'PROG'.
          lv_object_name = lv_name_dbl.

          lv_call_type = 'PROG'.
          lv_call_object = lv_name_dbl.
        ELSE.

* Parametros para buscar os objetos dependentes
          lv_object_type = <envi_tab>-type.
          lv_object_name = <envi_tab>-object.

          lv_call_type = <envi_tab>-type.
          lv_call_object = <envi_tab>-object.
        ENDIF.

        READ TABLE t_envi_tab_all TRANSPORTING NO FIELDS
        WITH KEY call_type = lv_call_type
                 call_obj =  lv_call_object.

        IF sy-subrc IS NOT INITIAL.
***          o_requests->get_dependents(
***            i_obj_type = lv_object_type
***            i_object_name = lv_object_name ).

* TEMPORARIO
          o_requests->get_dependents( EXPORTING
            i_obj_type = lv_object_type
            i_object_name = lv_object_name
            CHANGING rt_envi_tab = rt_envi_tab ).
        ENDIF.

      ENDLOOP.
    ENDIF.

    rt_envi_tab = t_envi_tab_all.

  ENDMETHOD.                    "get_dependents

ENDCLASS.                    "lcl_main IMPLEMENTATION

START-OF-SELECTION.

  CREATE OBJECT o_requests.

  o_requests->main( ).
