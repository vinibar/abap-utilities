*&---------------------------------------------------------------------*
*& Report  ZVAB_REQUEST_DOWNLOAD
*&  - Download request to zip file
*&---------------------------------------------------------------------*
*& Developed by @vinibar
*&
*&---------------------------------------------------------------------*

report zvab_request_download.

data g_trkorr type trkorr.
data t_e070 type table of e070.
data gs_dir type cst_rswatch01_alv.
data t_bdcdata type table of bdcdata.

data lo_zip type ref to cl_abap_zip.

field-symbols <e070> like line of t_e070.

select-options: s_trkorr for g_trkorr obligatory no intervals.
parameters: p_dest type string obligatory.

at selection-screen  on value-request for p_dest.
  cl_gui_frontend_services=>directory_browse(
  changing
    selected_folder = p_dest
  ).

start-of-selection.

  call 'C_SAPGPARAM' id 'NAME'  field 'DIR_TRANS'
                     id 'VALUE' field gs_dir-dirname.

  data v_cofiles type string.
  data v_data type string.
  data v_request type string.
  data v_path_to_download type string.
  data: v_text    type string,
        v_message type msgv1,
        v_bar.


  find '\' in gs_dir-dirname.
  if sy-subrc is initial.
    v_bar = '\'.
  else.
    v_bar = '/'.
  endif.

  concatenate gs_dir-dirname v_bar 'cofiles' v_bar into v_cofiles.
  concatenate gs_dir-dirname v_bar 'data' v_bar into v_data.

  select * from e070
  into table t_e070
  where trkorr in s_trkorr.

  loop at t_e070 assigning <e070>.

* SOLUTIOIT - Douglas Samaniego - 02.03.2017 14:14:59 - Início
    clear lo_zip.
* SOLUTIOIT - Douglas Samaniego - 02.03.2017 14:14:59 - Fim

    if <e070>-trstatus <> 'R' and
       <e070>-trstatus <> 'N'.

      v_text = text-001.
      replace 'TRKORR' with <e070>-trkorr into v_text.
      message v_text type 'I'.
      continue.

    endif.

    v_request = <e070>-trkorr.
    replace sy-sysid in v_request with space.
    concatenate v_request sy-sysid into v_request
      separated by '.'.

    data lv_dest_path type string.

    " Donwload COFILES
    concatenate v_cofiles v_request into v_path_to_download.
    perform f_download using v_path_to_download v_request
            changing lv_dest_path.

    perform f_add_to_zip using <e070>-trkorr lv_dest_path space.

    " Download DATA
    replace regex '^\K' in v_request with 'R'.
    concatenate v_data v_request into v_path_to_download.
    perform f_download using v_path_to_download v_request
           changing lv_dest_path.

    perform f_add_to_zip using <e070>-trkorr lv_dest_path abap_true.

* SOLUTIOIT - Douglas Samaniego - 06.03.2017 08:56:14 - Início
    concatenate 'Arquivos gerados com muito sucesso ;)' '-'
               <e070>-trkorr
               into v_message separated by space.
* SOLUTIOIT - Douglas Samaniego - 06.03.2017 08:56:14 - Fim

    message v_message type 'I'.

  endloop.

*&---------------------------------------------------------------------*
*&      Form  f_download
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->I_FILE_APPL  text
*----------------------------------------------------------------------*
form f_download using i_file_appl i_filename changing c_dest.

  data lv_file type rcgiedial-iefile.
  data lv_file_size type drao-orln.
  data lv_lines type i.
  data lt_data type table of rcgrepfile.
  data lv_dest type string.

  concatenate p_dest i_filename into lv_dest separated by '\'.
  lv_file = i_file_appl.

  perform bdc_dynpro using 'SAPLC13Z' '1010'.
  perform bdc_field using 'bdc_okcode'  '=EEXO'.
  perform bdc_field using 'rcgfiletr-ftfront'  lv_dest.
  perform bdc_field using 'rcgfiletr-ftappl' lv_file.
  perform bdc_field using 'rcgfiletr-ftftype'	'bin'.

  data lv_params type ctu_params.

  lv_params-defsize = 'X'.
  lv_params-dismode = 'N'.
  lv_params-updmode = 'S'.

  call transaction 'CG3Y' using t_bdcdata
        options from lv_params.
  clear t_bdcdata.

  c_dest = lv_dest.

endform.                    "f_download

*&---------------------------------------------------------------------*
*&      Form  f_add_to_zip
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->U_DEST     Destination Path
*      -->U_DOWNLOAD Download after add?
*----------------------------------------------------------------------*
form f_add_to_zip using u_request u_dest u_download.

*  STATICS lo_zip TYPE REF TO cl_abap_zip.
  data lv_filelength type i.
  data lv_xstring type xstring.
  data lv_file_name type string.
  data lv_file_path type string.
  data lv_zip_file_name type string.
  data lv_rc type sy-subrc.
  data lt_bin_file type table of raw255.
  data lv_text      type as4text.

  if lo_zip is not bound.
    create object lo_zip.
  endif.

  cl_gui_frontend_services=>gui_upload(
    exporting
      filename                = u_dest
      filetype                = 'BIN'
    importing
      filelength              = lv_filelength
    changing
      data_tab                = lt_bin_file ).

  call function 'SCMS_BINARY_TO_XSTRING'
    exporting
      input_length = lv_filelength
    importing
      buffer       = lv_xstring
    tables
      binary_tab   = lt_bin_file.

  call function 'SO_SPLIT_FILE_AND_PATH'
    exporting
      full_name     = u_dest
    importing
      stripped_name = lv_file_name
      file_path     = lv_file_path
    exceptions
      x_error       = 1
      others        = 2.

  cl_gui_frontend_services=>file_delete(
      exporting
        filename = u_dest
      changing
        rc = lv_rc
  ).

  lo_zip->add(
    name = lv_file_name
    content = lv_xstring
  ).

  if u_download is not initial.

    lv_xstring = lo_zip->save( ).

* SOLUTIOIT - Douglas Samaniego - 01.03.2017 09:08:02 - Início
* Busca descrição da Request
    clear lv_text.
    select single as4text
      from e07t
      into lv_text
      where trkorr = u_request
        and langu  = sy-langu.

    if sy-subrc <> 0.
      clear lv_text.
    endif.
* SOLUTIOIT - Douglas Samaniego - 01.03.2017 09:08:02 - Fim

* SOLUTIOIT - Douglas Samaniego - 01.03.2017 09:12:54 - Início
*    concatenate lv_file_path u_request
    concatenate lv_file_path u_request+4
      into lv_file_path separated by '\'.

    concatenate lv_file_path '-' lv_text
           into lv_file_path separated by space.
* SOLUTIOIT - Douglas Samaniego - 01.03.2017 09:13:06 - Fim

    concatenate lv_file_path 'ZIP'
      into lv_file_path separated by '.'.

    call function 'SCMS_XSTRING_TO_BINARY'
      exporting
        buffer        = lv_xstring
      importing
        output_length = lv_filelength
      tables
        binary_tab    = lt_bin_file.

    cl_gui_frontend_services=>gui_download(
      exporting
        bin_filesize              = lv_filelength
        filename                  = lv_file_path
        filetype                  = 'BIN'
      changing
        data_tab                  = lt_bin_file ).

  endif.


endform                    "f_add_to_zip
.                    "f_add_to_zip

*&---------------------------------------------------------------------*
*&      Form  BDC_DYNPRO
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->PROGRAM    text
*      -->DYNPRO     text
*----------------------------------------------------------------------*
form bdc_dynpro using program dynpro.
  data ls_bdcdata like line of t_bdcdata.
  ls_bdcdata-program = program.
  ls_bdcdata-dynpro = dynpro.
  ls_bdcdata-dynbegin = 'X'.
  append ls_bdcdata to t_bdcdata.
endform.                    "BDC_DYNPRO

*&---------------------------------------------------------------------*
*&      Form  BDC_FIELD
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->FNAM       text
*      -->FVAL       text
*----------------------------------------------------------------------*
form bdc_field using fnam fval.
  data ls_bdcdata like line of t_bdcdata.
  if fval <> ' '.
    ls_bdcdata-fnam = fnam.
    ls_bdcdata-fval = fval.
    append ls_bdcdata to t_bdcdata.
  endif.
endform.                    "BDC_FIELD
